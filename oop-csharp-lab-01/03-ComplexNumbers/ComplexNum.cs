﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return (Math.Sqrt(Math.Pow(this.Re, 2) + Math.Pow(this.Im, 2))); 
                //throw new NotImplementedException();
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return ( new ComplexNum(this.Re , -this.Im));
                //throw new NotImplementedException();
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"{this.GetType().Name}(RE={this.Re}, IM={this.Im})";
            //throw new NotImplementedException();
        }

        public ComplexNum cSum(ComplexNum c1) {
            return new ComplexNum(this.Re + c1.Re, this.Im + c1.Im);
        }

        public ComplexNum cSub(ComplexNum c1)
        {
            return new ComplexNum(this.Re - c1.Re, this.Im - c1.Im);
        }

        public ComplexNum cMul(ComplexNum c1)
        {
            return new ComplexNum(this.Re * c1.Re, this.Im * c1.Im);
        }

        public ComplexNum cDiv(ComplexNum c1)
        {
            return new ComplexNum(this.Re / c1.Re, this.Im / c1.Im);
        }

        public ComplexNum Invert() {
            return (new ComplexNum(1/this.Re , 1/this.Im));
        }

    }
}
