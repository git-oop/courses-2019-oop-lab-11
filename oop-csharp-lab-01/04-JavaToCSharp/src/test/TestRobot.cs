using System;

namespace JavaToCSharp.test
{
    public class TestRobot
    {
        private static void assertEquality(String propertyName, Object expected, Object actual)
        {
            if (actual == null || !actual.Equals(expected))
            {
                Console.WriteLine(propertyName + " was expected to be " + expected
                                  + ", but it yields " + actual + " (ERROR!)");
            }
            else
            {
                Console.WriteLine(propertyName + ": " + actual + " (CORRECT)");
            }
        }
    }
    
    
    static void main( String[] args) {
        int stepsDefault = (int) (BaseRobot.BATTERY_FULL / BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
        var r0 = new BaseRobot("R2D2");
        String r0pos = r0 + " position";
        String r0bat = r0 + " battery";
        assertEquality(r0pos, new RobotPosition(0, 0), r0.getPosition());
        assertEquality(r0bat, BaseRobot.BATTERY_FULL, r0.getBatteryLevel());
        var steps = stepsDefault;
            while (r0.moveUp()) {
            steps--;
        }
        if (steps == 0) {
            assertEquality(r0bat, true, r0.getBatteryLevel() < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
            r0.recharge();
            assertEquality(r0bat, BaseRobot.BATTERY_FULL, r0.getBatteryLevel());
            steps = stepsDefault;
        } else {
            assertEquality(r0pos, new RobotPosition(0, RobotEnvironment.Y_UPPER_LIMIT), r0.getPosition());
        }
        while (r0.moveRight()) {
            steps--;
        }
        if (steps == 0) {
            assertEquality(r0bat, true, r0.getBatteryLevel() < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
            r0.recharge();
            assertEquality(r0bat, BaseRobot.BATTERY_FULL, r0.getBatteryLevel());
            steps = stepsDefault;
        } else {
            assertEquality(r0pos,
                new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT),
                r0.getPosition());
        }
        while (r0.moveRight()) {
            steps--;
        }
        if (steps == 0) {
            assertEquality(r0bat, true, r0.getBatteryLevel() < BaseRobot.MOVEMENT_DELTA_CONSUMPTION);
            r0.recharge();
            assertEquality(r0bat, BaseRobot.BATTERY_FULL, r0.getBatteryLevel());
            steps = stepsDefault;
        } else {
            assertEquality(r0pos,
                new RobotPosition(RobotEnvironment.X_UPPER_LIMIT, RobotEnvironment.Y_UPPER_LIMIT),
                r0.getPosition());
        }

    }
}
