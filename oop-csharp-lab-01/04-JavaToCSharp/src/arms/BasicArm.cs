using System;

namespace JavaToCSharp.arms
{
    public class BasicArm
    {
        private static readonly double ENERGY_REQUIRED_TO_MOVE = 0.2; //readonly instead of final
        private static readonly double ENERGY_REQUIRED_TO_GRAB = 0.1;
        
        bool grabbing;
        readonly String name;

        public BasicArm ( String name)
        {
            this.name = name;
        }

        //property
        public bool Grabbing
        {
            get { return grabbing; }
        }

        public void pickUp() {
            grabbing = true;
        }

        public void dropDown() {
            grabbing = false;
        }
        public double getConsuptionForPickUp() {
            return ENERGY_REQUIRED_TO_MOVE + ENERGY_REQUIRED_TO_GRAB;
        }

        public double getConsuptionForDropDown() {
            return ENERGY_REQUIRED_TO_MOVE;
        }

        public string Name => name;
    }
}