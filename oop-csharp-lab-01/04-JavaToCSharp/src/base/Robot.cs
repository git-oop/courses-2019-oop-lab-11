using System;

namespace JavaToCSharp
{
    public interface Robot
    {
        /**
     * Moves the robot up by one unit
     * 
     * @return true if a movement has been performed
     */
        bool moveUp();

        /**
         * Moves the robot down by one unit
         * 
         * @return true if a movement has been performed
         */
        bool moveDown();

        /**
         * Moves the robot left by one unit
         * 
         * @return true if a movement has been performed
         */
        bool moveLeft();

        /**
         * Moves the robot right by one unit
         * 
         * @return true if a movement has been performed
         */
        bool moveRight();

        /**
         * Fully recharge the robot
         */
        void recharge();

        /**
         * @return The robot's current battery level
         */
        double getBatteryLevel();

        /**
         * @return The robot environment
         */
        Position2D getPosition();
    }
}