using System;

namespace JavaToCSharp
{
    public class RobotPosition : Position2D
    {
        private readonly int x;
        private readonly int y;

        /**
         * @param x
         *            X position
         * @param y
         *            Y position
         */
        public RobotPosition( int x,  int y) {
            this.x = x;
            this.y = y;
        }

        public bool equals( Object o) {
            if (o is Position2D) {
                 var p = (Position2D) o;
                return x == p.getX() && y == p.getY();
            }
            return false;
        }

        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }

        public int hashCode() {
            /*
             * This could be implemented WAY better.
             */
            return x ^ y;
        }

        public RobotPosition sumVector( int x,  int y) {
            return new RobotPosition(this.x + x, this.y + y);
        }

        public RobotPosition sumVector( Position2D p) {
            return new RobotPosition(x + p.getX(), y + p.getY());
        }

        public String toString() {
            return "[" + x + ", " + y + "]";
        }
    }
}