using System;

namespace JavaToCSharp
{
    public interface Position2D
    {
        /**
     * @return X position
     */
        int getX();

        /**
         * @return Y position
         */
        int getY();

        /**
         * @param p
         *            delta movement to sum
         * @return the new position
         */
        RobotPosition sumVector(Position2D p);

        /**
         * @param x
         *            X delta
         * @param y
         *            Y delta
         * @return the new position
         */
        RobotPosition sumVector(int x, int y);

    }
}