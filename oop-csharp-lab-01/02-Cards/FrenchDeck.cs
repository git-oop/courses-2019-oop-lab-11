﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] frenchCards;

        public FrenchDeck()
        {
            frenchCards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + Enum.GetNames(typeof(JollySeed)).Length*4];
        }

        public void Initialize()
        {
            int i = 0;
            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {

                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    frenchCards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
                frenchCards[i] = new Card(JollySeed.JOLLY.ToString());
                i++;
            }
        }

        public Card this[FrenchSeed seme, FrenchValue value]
        {
            get { return this.frenchCards[(int)seme * 10 + (int)value]; }
        }

        public void Print()
        {
            foreach (Card card in frenchCards)
            {
                Console.WriteLine(card.ToString());
            }
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */

            //throw new NotImplementedException();
        }




        enum JollySeed{
            JOLLY
        }

        
    }

    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
